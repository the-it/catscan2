<?PHP

require_once ( 'php/common.php' ) ;

$debugging_output = 0 ;
$debugging_time = microtime(true) ;
$omniscan_temp_dir = getcwd()."/../tmp" ;
#$db = openDB ( 'wikidata' , 'wikidata' ) ; // For get_db_safe

// Deletes temp files 1h and older
function cleanOldOmniscanTempFiles () {
	global $omniscan_temp_dir ;
	$cmd = 'find $omniscan_temp_dir -daystart -maxdepth 1 -mmin +59 -type f -name "omniscan_*" -exec rm -f {} \;' ;
}

function getOmiscanTempFile () {
	global $omniscan_temp_dir ;
	return tempnam ( $omniscan_temp_dir , "tool_" ) ;
}

/*
  This class abstracts a list of pages.
  These are either 
*/
class Pagelist {
	var $language ;
	var $project ;
	
	// PROTECTED
	var $file ;
	var $namespace_names = array() ;
	var $batch_fh ;
	var $append_fh ;
	var $out_fh ;
	var $batch_size ;
	var $mysql_timeout_min = 5 ;
	
	function __construct() {
		$this->batch_size = 10000 ;
		$this->batch_fh = false ;
		$this->append_fh = false ;
	}

	function __destruct() {
		$this->cleanup() ;
	}
	
	function debug ( $msg , $method = '' ) {
		global $debugging_output , $debugging_time ;
		if ( !$debugging_output ) return ;
		print "DEBUG: $msg" ;
		if ( $method != '' ) print " [$method]" ;
		$mt = microtime(true) ;
		print " (" . floor(($mt-$debugging_time)) . "ms)\n" ;
		$debugging_time = $mt ;
	}

	function error ( $msg , $method = '' ) {
		print "ERROR: $msg" ;
		if ( $method != '' ) print " [$method]" ;
		print "\n" ;
		exit ( 0 ) ;
	}
	
	function getTempFile() {
		global $omniscan_temp_dir ;
		return tempnam ( $omniscan_temp_dir , "omniscan_" ) ;
	}
	
	function blankFile ( $l , $p ) {
		$this->cleanup() ;
		$this->language = $l ;
		$this->project = $p ;
		$this->file = $this->getTempFile() ;
	}
	
	function appendPage ( $a ) {
		if ( false === $this->append_fh ) {
			if ( $this->file == '' ) $this->blankFile ( $this->language , $this->project ) ;
			if ( !file_exists($this->file) ) { error_log("getNextBatch: No file") ; return $ret ; }
			$this->debug ( "open" , "appendPage" ) ;
			$this->append_fh = @fopen ( $this->file , 'a' ) ;
		}
		fwrite ( $this->append_fh , implode ( "\t" , $a ) . "\n" ) ;
	}
	
	function stopAppend () {
		if ( false === $this->append_fh ) return ;
		fclose ( $this->append_fh ) ;
		$this->append_fh = false ;
	}
	
	function stopRead () {
		if ( false === $this->batch_fh ) return ;
		fclose($this->batch_fh) ;
		$this->batch_fh = false ;
	}
	
	function swap ( $other ) {
		foreach ( array ( 'language' , 'project' , 'file' , 'namespace_names' , 'append_fh' , 'batch_fh' , 'batch_size' ) AS $v ) {
			$tmp = $other->$v ;
			$other->$v = $this->$v ;
			$this->$v = $tmp ;
		}
	}
	
	function hasData() {
		return ( isset($this->file) and $this->file != '' ) ;
	}

	function cleanup () {
//		$this->debug ( "start" , "cleanup" ) ;
		$this->stopRead() ;
		$this->stopAppend() ;
		if ( $this->file != '' ) {
			unlink ( $this->file ) ;
			$this->file = '' ;
		}
//		$this->debug ( "end" , "cleanup" ) ;
	}
	
	function loadWikidataPagePile ( $pagepile ) { // historic interface...
		return $this->loadPagePile ( $pagepile , true ) ;
	}
	
	function loadPagePile ( $pagepile , $force_wikidata = false ) {
		require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;
		$this->debug ( "start" , "loadPagePile" ) ;
		$this->cleanup() ;
		$this->file = $this->getTempFile() ;
		$fh = fopen ( $this->file , 'w' ) ;
		
		$me = $this ;
		$pp = '' ;
		if ( is_object($pagepile) ) $pp = $pagepile ;
		else $pp = new PagePile ( $pagepile ) ;
		if ( $force_wikidata ) $pp = $pp->getAsWikidata() ;

		$lp = $pp->getLanguageProject() ;
		$me->language = $lp[0] ;
		$me->project = $lp[1] ;

		$pp->each ( function ( $o , $pp ) use ( &$fh ) {
			fwrite ( $fh , $o->page . "\t" . $o->ns . "\n" ) ;
		} ) ;
		fclose ( $fh ) ;
		
		$this->debug ( "end" , "loadPagePile" ) ;
	}
	
	function loadWDQ ( $query ) {
		global $wdq_internal_url ;
		$this->debug ( "start" , "loadWDQ" ) ;
		$this->cleanup() ;
		$this->language = 'wikidata' ;
		$this->project = 'wikidata' ;
		$tmp1 = $this->getTempFile() ;
		$this->file = $this->getTempFile() ;
		
#		$url = "http://wdq.wmflabs.org/api?q=" . urlencode($query) ;
		$url = "$wdq_internal_url?q=" . urlencode($query) ;
		$this->debug ( "querying URL" , "loadWDQ" ) ;
		file_put_contents($tmp1, fopen($url, 'r'));
		
		$this->debug ( "parsing/sorting" , "loadWDQ" ) ;
		$cmd = "jq '.items' $tmp1 | sed 's/[^0-9]//g' |  grep -v '^$' | sed -e 's/^/Q/' | sed -e 's/$/\t0/' | sort -u > " . $this->file ; //  | sort
		exec ( $cmd ) ;
		unlink ( $tmp1 ) ;
		$this->debug ( "end" , "loadWDQ" ) ;
	}

	// Generates page list with pages (any namespace) using any of the passed templates.
	function loadTemplates ( $o_params ) {
		$this->debug ( "start" , "loadTemplates" ) ;
		$this->cleanup() ;

		// Default settings
		$o = (object) array ( 'talk2main'=>0 ) ;
		
		// Overwrite defaults with passed parameters
		foreach ( $o_params AS $k => $v ) $o->$k = $v ;
		
		$db = openDB ( $o->language , $o->project ) ;
		$this->setDBoptions ( $db ) ;

		$templates = $o->templates ;
		if ( !is_array ( $templates ) ) $templates = array ( $templates ) ; // String to array
		foreach ( $templates AS $k => $v ) {
			$templates[$k] = $db->real_escape_string ( str_replace ( ' ' , '_' , $v ) ) ;
		}

		$this->file = $this->getTempFile() ;
		$tmp1 = $this->getTempFile() ;
		$fh = fopen ( $tmp1 , 'w' ) ;
		$sql = "SELECT DISTINCT page_namespace,page_title FROM page,templatelinks WHERE tl_from=page_id AND " ;
		$sql .= "tl_namespace=10 AND tl_title IN ('" . implode("','",$templates) . "')" ; // ORDER BY page_namespace,page_title" ;
		$this->last_sql = $sql ;
		if(!$result = $db->query($sql)) $this->error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		$cnt = 0 ;
		while($r = $result->fetch_object()){
			$r->page_namespace *= 1 ;
			if ( $o->talk2main and $r->page_namespace % 2 > 0 ) $r->page_namespace-- ;
			fwrite ( $fh , $r->page_title . "\t" . $r->page_namespace . "\n" ) ;
			$cnt++ ;
		}
		fclose ( $fh ) ;
		
		if ( $o->talk2main ) { // Remove duplicates (can happen with talk2main=1)
			$cmd = "sort -u $tmp1 > " . $this->file ;
			exec ( $cmd ) ;
			unlink ( $tmp1 ) ;
		} else {
			rename ( $tmp1 , $this->file ) ;
		}

		$this->debug ( "end" , "loadTemplates" ) ;
	}
	
	/*
		Parameter keys:
		* depth INT
		* root STRING or ARRAY[STRING]
		* namespace OR namespaces STRING or ARRAY[STRING]
		* redirects STRING "any", "none", "only"
		* talk2main INT 0/1
	*/
	function loadCategoryTree ( $o_params ) {
		global $db_single_con ;
		$this->debug ( "start" , "loadCategoryTree" ) ;
		$this->cleanup() ;
		
		// Default settings
		$o = (object) array ( 'depth'=>12 , 'namespaces'=>array(0) , 'redirects'=>'any' , 'talk2main'=>0 ) ;
		
		// Overwrite defaults with passed parameters
		foreach ( $o_params AS $k => $v ) $o->$k = $v ;
		
		// Sanity checks
		if ( isset ( $o->namespace ) ) $o->namespaces = $o->namespace ;
		if ( !is_array($o->namespaces) ) $o->namespaces = array($o->namespaces) ;
		
		$this->language = $o->language ;
		$this->project = $o->project ;

		$this->debug ( "params: ".json_encode($o) , "loadCategoryTree" ) ;

		$this->file = $this->getTempFile() ;

		$db = false ;
		if ( isset($db_single_con) ) {
			$db = $db_single_con ;
			$db->query("use ".$this->getSitename()."_p") ;
		} else $db = openDB ( $o->language , $o->project ) ;
		$this->setDBoptions ( $db ) ;
		if ( $db === false ) {
			return ;
		}
		
		$cats = array() ;
		$rc = $o->root ;
		if ( !is_array($rc) ) $rc = array ( $rc ) ;
		findSubcats ( $db , $rc , $cats , $o->depth ) ;

		$this->debug ( "using " . count($cats) . " category" , "loadCategoryTree" ) ;

		$tmp1 = $this->getTempFile() ;
		$fh = fopen ( $tmp1 , 'w' ) ;
		$sql = "SELECT DISTINCT page_namespace,page_title FROM page,categorylinks WHERE cl_from=page_id AND " ;
		$sql .= "page_namespace IN (" . implode(',',$o->namespaces) . ") AND cl_to IN ('" . implode("','",$cats) . "')" ; // ORDER BY page_namespace,page_title" ;
		if ( $o->redirects == 'none' ) $sql .= " AND page_is_redirect=0" ;
		if ( $o->redirects == 'only' ) $sql .= " AND page_is_redirect=1" ;
		if(!$result = $db->query($sql)) $this->error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		$cnt = 0 ;
		while($r = $result->fetch_object()){
			$r->page_namespace *= 1 ;
			if ( $o->talk2main and $r->page_namespace % 2 > 0 ) $r->page_namespace-- ;
			fwrite ( $fh , $r->page_title . "\t" . $r->page_namespace . "\n" ) ;
			$cnt++ ;
		}
		fclose ( $fh ) ;
		if ( !isset($db_single_con) ) $db->close() ;

		if ( $o->talk2main ) { // Remove duplicates (can happen with talk2main=1)
			$cmd = "sort -u $tmp1 > " . $this->file ;
			exec ( $cmd ) ;
			unlink ( $tmp1 ) ;
		} else {
			rename ( $tmp1 , $this->file ) ;
		}

		$this->debug ( "end" , "loadCategoryTree" ) ;
	}
	
	function loadNamespaceNames () {
		$this->debug ( "start" , "loadNamespaceNames" ) ;
		if ( count ( $this->namespace_names ) > 0 ) return ; // Had that
		$l = $this->language == 'wikidata' ? 'www' : $this->language ;
		$url = "http://$l." . $this->project . ".org/w/api.php?action=query&meta=siteinfo&siprop=namespaces|namespacealiases&format=json" ;
		$this->debug ( "Loading $url" , "loadNamespaceNames" ) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$star = '*' ;
		foreach ( $j->query->namespaces AS $n ) {
//			if ( isset ( $n->canonical ) ) $this->namespace_names[$n->id] = $n->canonical ;
//			else 
			$this->namespace_names[$n->id] = str_replace ( ' ' , '_' , $n->$star ) ; // Hmm. Use canonical or local namespace?
		}
		$this->debug ( "end" , "loadNamespaceNames" ) ;
	}
	
	function getPrefixedTitle ( $page , $ns ) {
		if ( count($this->namespace_names) == 0 ) $this->loadNamespaceNames () ;
		if ( $this->namespace_names[$ns] == '' ) return $page ;
		return $this->namespace_names[$ns] . ':' . $page ;
	}
	
	function getSitename () {
		$p = $this->project=='wikipedia'?'wiki':$this->project ;
		return $this->language . $p ;
	}
	
	function getNextBatch ( $return_prefixed_title = false ) {
		$this->debug ( "start" , "getNextBatch" ) ;
		$ret = array() ;
		if ( false === $this->batch_fh ) {
			if ( $this->file == '' ) { error_log("getNextBatch: Blank file name") ; return $ret ; }
			if ( !file_exists($this->file) ) { error_log("getNextBatch: No file") ; return $ret ; }
			$this->debug ( "open" , "getNextBatch" ) ;
			$this->batch_fh = fopen ( $this->file , 'r' ) ;
//			if ( !$this->batch_fh ) return array() ;
		} else {
			if ( feof($this->batch_fh) ) {
				fclose ( $this->batch_fh ) ;
				$this->batch_fh = false ;
				$this->debug ( "closing" , "getNextBatch" ) ;
				return $ret ;
			}
		}

		if ( false === $this->batch_fh ) return $ret ;
		while ( !feof($this->batch_fh) and count($ret) < $this->batch_size ) {
			$l = explode ( "\t" , trim ( fgets ( $this->batch_fh ) ) ) ;
			if ( count ( $l ) < 2 ) continue ; // Huh?
			if ( $return_prefixed_title ) $ret[] = $this->getPrefixedTitle ( $l[0] , $l[1] ) ;
			else $ret[] = $l ;
		}
		$this->debug ( "Read " . count($ret) . " lines" , "getNextBatch" ) ;

		if ( count($ret) == 0 and feof($this->batch_fh) ) {
			fclose($this->batch_fh) ;
			$this->batch_fh = false ;
		}
		
		$this->debug ( "end" , "getNextBatch" ) ;
		return $ret ;
	}
	
	function getWikidataItems () {
		$this->debug ( "start" , "getWikidataItems" ) ;
		if ( $this->language == 'wikidata' ) $this->error ( "Trying to der Wikidata items for Wikidata items." , 'getWikidataItems' ) ;
		if ( $this->file == '' ) $this->error ( "No file." , 'getWikidataItems' ) ;
		$ret = new Pagelist ;
		$ret->language = 'wikidata' ;
		$ret->project = 'wikidata' ;
		$tmp1 = $this->getTempFile() ;
		$db = openDB ( 'wikidata' , 'wikidata' ) ;
		$this->setDBoptions ( $db ) ;
		$sitename = $this->getSitename() ;

		$fh_out = fopen ( $tmp1 , 'w' ) ;
		while ( $pages = $this->getNextBatch(true) ) {
			foreach ( $pages AS $k => $v ) {
				$pages[$k] = $db->real_escape_string ( str_replace ( '_' , ' ' , $v ) ) ;
			}
			
			$sql = "select ips_item_id from wb_items_per_site WHERE ips_site_id='$sitename' and ips_site_page IN ('" . implode("','",$pages) . "')" ;
			if(!$result = $db->query($sql)) $this->error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
			while($r = $result->fetch_object()){
				fwrite ( $fh_out , 'Q' . $r->ips_item_id . "\t0\n" ) ;
			}
		}
		fclose ( $fh_out ) ;
		
		$ret->file = $tmp1 ;
		$this->debug ( "end" , "getWikidataItems" ) ;
		return $ret ;
	}
	
	/*
		Finds redlinks in the listed pages
		WARNING: The file in the return object has a third column, it will NOT work correctly with subsequent operations unless filtered!
	*/
	function getRedlinks ( $o_params ) {
		$this->debug ( "start" , "getRedlinks" ) ;
		$o = (object) array ( 'no_templates'=>0 , 'namespaces'=>array() , 'min'=>0 ) ;
		foreach ( $o_params AS $k => $v ) $o->$k = $v ;
		if ( !is_array ( $o->namespaces ) ) $o->namespaces = array ( $o->namespaces ) ;
		$db = openDB ( $this->language , $this->project ) ;
		$this->setDBoptions ( $db ) ;
		
		$tmp1 = $this->getTempFile() ;
		$fh_out = fopen ( $tmp1 , 'w' ) ;
		while ( $a = $this->getNextBatch() ) {
			$b = array() ;
			foreach ( $a AS $v ) $b[$v[1]][] = $db->real_escape_string ( $v[0] ) ;
			foreach ( $b AS $ns => $pages ) {
				$sql = "SELECT pl_namespace,pl_title FROM pagelinks pl1,page WHERE pl_from=page_id AND page_namespace=$ns AND page_title IN ('" . implode("','",$pages) . "') " ;
				if ( count($o->namespaces) > 0 ) $sql .= "AND pl_namespace IN (" . implode(',',$o->namespaces) . ") " ;
				$sql .= "AND NOT EXISTS (SELECT * FROM page p2 WHERE p2.page_namespace=pl_namespace AND p2.page_title=pl_title LIMIT 1)" ;
				if ( $o->no_templates ) {
					$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks pl2 WHERE pl2.pl_from_namespace=14 AND pl2.pl_namespace=pl1.pl_namespace AND pl2.pl_title=pl1.pl_title limit 1)" ;
				}
//				print "<pre>$sql</pre>" ;
				if(!$result = $db->query($sql)) $this->error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
				while($r = $result->fetch_object()){
					fwrite ( $fh_out , $r->pl_title."\t".$r->pl_namespace."\n" ) ;
				}
			}
		}
		fclose ( $fh_out ) ;
		
		// Min filter
		$tmp2 = $this->getTempFile() ;
		$cmd = "sort $tmp1 | uniq -c | sort -nr > $tmp2" ;
		exec ( $cmd ) ;
		$fh_in = fopen ( $tmp2 , 'r' ) ;
		$fh_out = fopen ( $tmp1 , 'w' ) ;
		while ( !feof($fh_in) ) {
			$l = fgets($fh_in) ;
			if ( !preg_match ( '/^\s*(\d+)\s(.+?)\s(\d+)$/' , $l , $m ) ) continue ;
			if ( $m[1]*1 < $o->min*1 ) continue ;
			fwrite ( $fh_out , $m[2]."\t".$m[3]."\t".$m[1]."\n" ) ;
		}
		fclose ( $fh_out ) ;
		fclose ( $fh_in ) ;
		unlink ( $tmp2 ) ;

		$ret = new Pagelist ;
		$ret->language = $this->language ;
		$ret->project = $this->project ;
		$ret->file = $tmp1 ;
//		print "<pre>" . $ret->wcl() . "</pre>" ;

		$this->debug ( "end" , "getRedlinks" ) ;
		return $ret ;
	}


	function setDBoptions ( &$db ) {
		$db->options ( MYSQLI_OPT_CONNECT_TIMEOUT , 60*$this->mysql_timeout_min ) ;
	}
	
	function subset ( $pl1 , $pl2 ) {
		$this->debug ( "start" , "subset" ) ;
		$tmp1 = $this->getTempFile() ;
		if ( !file_exists($pl1->file) ) error_log ( "subset: file 1 \"" . $pl1->file . "\" does not exist!" ) ;
		if ( !file_exists($pl2->file) ) error_log ( "subset: file 2 \"" . $pl2->file . "\" does not exist!" ) ;
		$cmd = "cat " . $pl1->file . " " . $pl2->file . " | sort | uniq -d > " . $tmp1 ;
		$this->debug ( "CMD $cmd" , "subset" ) ;
		exec ( $cmd ) ;
		$this->cleanup() ;
		$this->file = $tmp1 ;
		$this->language = $pl1->language ;
		$this->project = $pl1->project ;
		$this->debug ( "end" , "subset" ) ;
	}
	
	function union ( $pl1 , $pl2 ) {
		$this->debug ( "start" , "union" ) ;
		$tmp1 = $this->getTempFile() ;
		if ( !file_exists($pl1->file) ) error_log ( "union: file 1 \"" . $pl1->file . "\" does not exist!" ) ;
		if ( !file_exists($pl2->file) ) error_log ( "union: file 2 \"" . $pl2->file . "\" does not exist!" ) ;
		$cmd = "cat " . $pl1->file . " " . $pl2->file . " | sort -u > " . $tmp1 ;
		$this->debug ( "CMD $cmd" , "union" ) ;
		exec ( $cmd ) ;
		$this->cleanup() ;
		$this->file = $tmp1 ;
		$this->language = $pl1->language ;
		$this->project = $pl1->project ;
		$this->debug ( "end" , "union" ) ;
	}
	
	function only_in_list_1 ( $pl1 , $pl2 ) {
		$this->debug ( "start" , "only_in_list_1" ) ;

		if ( !file_exists($pl1->file) ) error_log ( "only_in_list_1: file 1 \"" . $pl1->file . "\" does not exist!" ) ;
		if ( !file_exists($pl2->file) ) error_log ( "only_in_list_1: file 2 \"" . $pl2->file . "\" does not exist!" ) ;

		$tmp2 = $this->getTempFile() ;
		$cmd = "cat " . $pl1->file . " " . $pl2->file . " | sort | uniq -d > " . $tmp2 ;
		$this->debug ( "CMD $cmd" , "only_in_list_1" ) ;
		exec ( $cmd ) ;
		
		$tmp1 = $this->getTempFile() ;
		$cmd = "cat " . $pl1->file . " " . $tmp2 . " | sort | uniq -u > " . $tmp1 ;
		$this->debug ( "CMD $cmd" , "only_in_list_1" ) ;
		exec ( $cmd ) ;
		unlink ( $tmp2 ) ;
		
		$this->cleanup() ;
		$this->file = $tmp1 ;
		$this->language = $pl1->language ;
		$this->project = $pl1->project ;
		$this->debug ( "end" , "only_in_list_1" ) ;
	}
	
	function duplicate () {
		$ret = new Pagelist ;
		$ret->language = $this->language ;
		$ret->project = $this->project ;
		$ret->file = $this->getTempFile() ;
		copy ( $this->file , $ret->file ) ;
		return $ret ;
	}
	
	function asRawArray () {
		if ( $this->file == '' ) return array();//$this->error ( "No file" , "asPrefixedArray" ) ;
		return file($this->file, FILE_IGNORE_NEW_LINES);
	}
	
	function wcl () {
		if ( $this->file == '' ) return 0 ;
		if ( !file_exists($this->file) ) return 0 ;
		exec ( 'wc -l ' . $this->file , $a ) ;
		return $a[0] * 1 ;
	}

}

?>