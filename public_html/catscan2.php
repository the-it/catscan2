<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'omniscan.inc' ) ;
require_once ( 'php/pagepile.php' ) ;

header("Connection: close");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
ini_set('memory_limit','150M');
set_time_limit ( 60 * 10 ) ; // Seconds
ini_set('user_agent','OmniScan (Magnus)'); # Fake user agent

if ( isset($_REQUEST['wiki']) ) exit ( 0 ) ; // Some weird bot calling...

class CatScan3 {

	var $interface_language = 'en' ; // TODO
	var $basecats = array () ;
	var $negcats = array () ;
	
	function run () {
		$this->start_time = microtime(true) ;
		$this->ctx = stream_context_create(array('http' => array('timeout' => 5)));
		$this->interface_language = $this->getParameter ( 'interface_language' , 'en' ) ;
		$this->i18n_url = 'http://meta.wikimedia.org/w/index.php?title=CatScan2/Interface' ;
		$this->getParameters() ;
		$this->loadLocale () ;
		$this->printHTML() ;
		$this->runQuery() ;
	}

	function getloc ( $s ) {
		if ( isset ( $this->loc[$s] ) ) return $this->loc[$s] ;
		return "<i>Missing:$s</i>" ;
	}
	
	function doIt () {
		if ( isset($this->clo['doit']) ) return true ;
		return isset($_REQUEST['doit']) ;
	}



	function loadLocale () {
	
		// Load interface language from wiki page
		$text = file_get_contents ( $this->i18n_url . "&action=raw" , 0 , $this->ctx ) ;
		$text = explode ( "\n" , $text ) ;
		$loc = array () ;
		$lang = '' ;
		foreach ( $text AS $line ) {
			$line = trim ( $line ) ;
			if ( substr ( $line , 0 , 2 ) == '==' ) {
				$lang = trim ( strtolower ( str_replace ( '=' , '' , $line ) ) ) ;
				$loc[$lang] = array () ;
			} else if ( substr ( $line , 0 , 1 ) == ';' ) {
				$line = trim ( substr ( $line , 1 ) ) ;
				$l = explode ( ':' , $line , 2 ) ;
				if ( count ( $l ) != 2 ) continue ;
				$loc[$lang][trim(strtolower($l[0]))] = $l[1] ;
			}
		}
		$this->available_interface_languages = array_keys ( $loc ) ;

		if ( isset ( $loc[$this->interface_language] ) ) $this->loc = $loc[$this->interface_language] ;
		else $this->loc = $loc['en'] ; // Fallback
		
		// Namespaces namespaces
		$this->namespaces_en = array () ;
		$this->namespaces = array () ;
		
		$api_url = "http://{$this->language}.{$this->project}.org/w/api.php" ;
		$url = "{$api_url}?action=query&meta=siteinfo&siprop=general|namespaces|namespacealiases|statistics&format=php" ;
		$data = unserialize ( file_get_contents ( $url , 0 , $this->ctx ) ) ;
		$data = $data['query']['namespaces'] ;
//		print "<pre>" ; print_r ( $data ) ; print "</pre>" ;
		
		foreach ( $data AS $num => $d ) {
//			print "<pre>" ; print_r ( $d ) ; print "</pre>" ;
			unset ( $v ) ;
			unset ( $vc ) ;
			if ( isset ( $d['*'] ) ) $v = $d['*'] ;
			if ( isset ( $d['canonical'] ) ) $vc = $d['canonical'] ;
			if ( $num == 0 ) {
				if ( !isset ( $v ) || $v == '' ) $v = $this->loc['namespace_0'] ;
				if ( !isset ( $vc ) || $vc == '' ) $vc = $loc['en']['namespace_0'] ;
			}
//			print "$v / $vc<hr/>" ;
			if ( isset ( $v ) ) $this->namespaces[$num] = $v ;
			if ( isset ( $vc ) ) $this->namespaces_en[$num] = $vc ;
		}
		
		
		foreach ( $this->loc AS $k => $v ) { // Legacy cleanup
			if ( substr ( $k , 0 , 10 ) != 'namespace_' ) continue ;
			unset ( $this->loc[$k] ) ;
		}


		if ( $this->interface_language != 'en' ) {
			$has_untranslated = false ;
			foreach ( $loc['en'] AS $k => $v ) {
				if ( substr ( $k , 0 , 10 ) != 'namespace_' ) continue ;
				if ( isset ( $this->loc[$k] ) ) continue ;
				$this->loc[$k] = "<i>!" . $v . "!</i>" ;
				$has_untranslated = true ;
//				print "UNSET $k<br/>\n" ;
			}
//			if ( $has_untranslated ) print "<div>ATTENTION: This page has untranslated interface text in " . $this->interface_language . ".</div>" ;
		}
/*
		foreach ( $loc['en'] AS $k => $v ) {
			if ( substr ( $k , 0 , 10 ) != 'namespace_' ) continue ;
			$nsn = substr ( $k , 10 ) ;
			$this->namespaces_en[$nsn] = $v ;
//			unset ( $this->loc[$k] ) ;
		}
		
		foreach ( $this->loc AS $k => $v ) {
			if ( substr ( $k , 0 , 10 ) != 'namespace_' ) continue ;
			$nsn = substr ( $k , 10 ) ;
			$this->namespaces[$nsn] = $v ;
			unset ( $this->loc[$k] ) ;
		}
*/		
		$this->sort_modes = array ( 'none' , 'title' , 'ns_title' , 'size' , 'date' , 'filesize' , 'uploaddate' , 'incoming_links' ) ;

		$this->formats = array ( 
			'html' => 'HTML' ,
			'csv' => 'CSV' ,
			'tsv' => 'TSV' ,
			'wiki' => 'Wiki' ,
//			'php' => 'PHP' ,
//			'xml' => 'XML' ,
			'json' => 'JSON' ,
//			'gallery' => 'Gallery' ,
		) ;
		
		global $pagepile_enabeled ;
		if ( $pagepile_enabeled ) $this->formats['pagepile'] = 'Pagepile' ;


		// Initialize language
		if ( $this->language == $this->interface_language ) {
			if ( isset ( $loc[$this->interface_language] ) ) {
				if ( isset ( $loc[$this->interface_language]['auto_lang'] ) ) {
					$this->language = $loc[$this->interface_language]['auto_lang'] ;
				}
			}
		}
		
	}
	
	
	function getParameter ( $key , $default = '' ) {
		if ( $default == '' and isset ( $this->default_parameter[$key] ) ) $default = $this->default_parameter[$key] ;
		if ( !isset ( $_REQUEST[$key] ) ) {
			if ( isset ( $this->clo[$key] ) ) return $this->clo[$key] ; // Command line parameter?
			return $default ;
		}
		$v = $_REQUEST[$key] ;
		return $v ;
	}


	function getParameters () {
	
//		$this->initCommandLineParameters() ;
		
		$this->templates = array ( 'templates_yes' , 'templates_any' , 'templates_no' ) ;
		$this->outlinks = array ( 'outlinks_yes' , 'outlinks_any' , 'outlinks_no' ) ;
		
		// Set default values
		$this->default_parameter = array (
			'project' => 'wikipedia' ,
			'depth' => 0 ,
			'format' => 'html' ,
			'sortby' => 'none' ,
			'sortorder' => 'ascending' ,
			'atleast_count' => 0 ,
			'min_redlink_count' => 1 ,
			'show_redirects' => 'both' ,
			'min_topcat_count' => 1 ,
			'edits' => array ( 'bots' => 'both' , 'anons' => 'both' , 'flagged' => 'both' ) ,
		) ;

		// Get misc parameters
		$this->language = $this->getParameter ( 'language' , $this->interface_language ) ;
		$this->project = $this->getParameter ( 'project' ) ;
		$this->depth = floor ( $this->getParameter ( 'depth' ) ) ;
		$this->format = $this->getParameter ( 'format' ) ;
		$this->before = $this->getParameter ( 'before' ) ;
		$this->after = $this->getParameter ( 'after' ) ;
		$this->max_age = $this->getParameter ( 'max_age' ) ;
		$this->only_new = $this->getParameter ( 'only_new' , false ) ;
		$this->larger = $this->getParameter ( 'larger' ) ;
		$this->smaller = $this->getParameter ( 'smaller' ) ;
		$this->minlinks = $this->getParameter ( 'minlinks' ) ;
		$this->maxlinks = $this->getParameter ( 'maxlinks' ) ;
		$this->sort_by = $this->getParameter ( 'sortby' ) ;
		$this->sort_order = $this->getParameter ( 'sortorder' ) ;
		$this->atleast_count = floor ( $this->getParameter ( 'atleast_count' ) ) ;
		$this->ext_image_data = $this->getParameter ( 'ext_image_data' , !$this->doIt() ) ;
		$this->file_usage_data = $this->getParameter ( 'file_usage_data' , !$this->doIt() ) ;

		$this->show_topcats = isset ( $_REQUEST['show_topcats'] ) ;
		$this->show_topcats_only = isset ( $_REQUEST['show_topcats_only'] ) ;
		$this->topcats_no_talk = isset ( $_REQUEST['topcats_no_talk'] ) ;
		$this->min_topcat_count = $this->getParameter ( 'min_topcat_count' ) ;
		
		$this->get_q = isset ( $_REQUEST['get_q'] ) ;

		$this->show_redlinks = isset ( $_REQUEST['show_redlinks'] ) ;
		$this->show_redlinks_only = isset ( $_REQUEST['show_redlinks_only'] ) ;
		$this->remove_template_redlinks = isset ( $_REQUEST['remove_template_redlinks'] ) ;
		$this->article_redlinks_only = isset ( $_REQUEST['article_redlinks_only'] ) ;
		$this->min_redlink_count = $this->getParameter ( 'min_redlink_count' ) ;

		$this->show_redirects = $this->getParameter ( 'show_redirects' ) ;
		$this->templates_use_talk_yes = isset ( $_REQUEST['templates_use_talk_yes'] ) ;
		$this->templates_use_talk_any = isset ( $_REQUEST['templates_use_talk_any'] ) ;
		$this->templates_use_talk_no = isset ( $_REQUEST['templates_use_talk_no'] ) ;
		
		if ( $this->language == 'commons' ) $this->project = 'wikimedia' ;

		$this->target_asset = $this->getParameter ( 'target_asset' , '' ) ;
		if ( $this->target_asset != '' ) {
			file_get_contents ( $this->wpipe_url . '?action=start&asset=' . $this->target_asset , 0 , $this->ctx ) ;
			$this->format = 'asset' ;
		}
		
		foreach ( $this->templates AS $t ) {
			$u = explode ( "\n" , trim ( $this->getParameter ( $t , '' ) ) ) ;
			if ( count ( $u ) == 1 and $u[0] == '' ) $u = array () ;
			$this->$t = $u ;
		}
		
		foreach ( $this->outlinks AS $o ) {
			$u = explode ( "\n" , trim ( $this->getParameter ( $o , '' ) ) ) ;
			if ( count ( $u ) == 1 and $u[0] == '' ) $u = array () ;
			$this->$o = $u ;
		}
		
		
		
		$simple_ns = $this->getParameter ( 'simple_ns' , '' ) ;
		if ( isset ( $_REQUEST['ns'] ) ) $this->ns = $_REQUEST['ns'] ;
		else if ( $simple_ns != '' ) {
			$this->ns = array () ;
			foreach ( explode ( ',' , $simple_ns ) AS $dummy => $nsn ) {
				$this->ns[$nsn] = 1 ;
			}
		} else $this->ns = array ( 0 => 1 ) ;

		$simple_comb = $this->getParameter ( 'simple_comb' , '' ) ;
		if ( isset ( $_REQUEST['comb'] ) ) $this->comb = $_REQUEST['comb'] ;
		else if ( $simple_comb != '' ) $this->comb = array ( $simple_comb => '1' ) ;
		else $this->comb = array ( 'subset' => '1' ) ;

		if ( isset ( $_REQUEST['edits'] ) ) $this->edits = $_REQUEST['edits'] ;
		else $this->edits = $this->default_parameter['edits'] ;
		foreach ( $this->default_parameter['edits'] AS $e ) {
			if ( !isset ( $this->edits[$e] ) ) $this->edits[$e] = 'both' ;
		}
	
		// Get categories, either as multiline text or as category1/category2
		$categories = $this->getParameter ( 'categories' , '' ) ;
		$categories = explode ( "\n" , $categories ) ;
		$categories[] = $this->getParameter ( 'category1' , '' ) ;
		$categories[] = $this->getParameter ( 'category2' , '' ) ;
		foreach ( $categories AS $c ) {
			$c = trim ( $c ) ;
			if ( $c == '' ) continue ;
			$this->basecats[] = $c ;
		}
		
		// Negative categories
		$negcats = $this->getParameter ( 'negcats' , '' ) ;
		$negcats = explode ( "\n" , $negcats ) ;
		foreach ( $negcats AS $c ) {
			$c = trim ( $c ) ;
			if ( $c == '' ) continue ;
			$this->negcats[] = $c ;
		}

		$this->basecats_orig = $this->basecats ;
		$this->negcats_orig = $this->negcats ;
	}
	
	function getNamespaceTable ( $min , $max ) {
		$ne = '' ;
		$no = '' ;
		foreach ( $this->namespaces AS $num=> $name ) {
			if ( $num < $min ) continue ;
			if ( $num > $max ) continue ;
			$checked = isset ( $this->ns[$num] ) ? 'checked' : '' ;
			$s = "<td nowrap><input type='checkbox' name='ns[$num]' id='ns_$num' value='1' $checked /><label for='ns_$num'>$name</label></td>" ;
			if ( $num % 2 == 0 ) $ne .= $s ;
			else $no .= $s ;
		}
		print "<table><tr>$ne</tr></tr>$no</tr></table>" ;
	}
	
	function getInputGroup ( $label , $name , $placeholder , $value ) {
		return '<div class="form-group" style="margin-right:20px"><div class="input-group">
		<div class="input-group-addon">'.$label.'</div>
		<input class="form-control" type="text" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'"></div></div>' ;
	}

	function getRadioGroup ( $name , $selected , $items ) {
		$ret = '<div class="btn-group" data-toggle="buttons">' ;
		foreach ( $items AS $key => $label ) {
			$ret .= '<label class="btn btn-primary ' . ($key==$selected?'active':'') . '">' ;
			$ret .= '<input type="radio" name="'.$name.'" value="'.$key.'" '.($key==$selected?'checked':'').'> ' . $label ;
			$ret .= '</label>' ;
		}
		$ret .= '</div>' ;
		return $ret ;
	}

	function printHTML () {
		if ( $this->doIt() and $this->format != 'html' ) return ;
		$script = array_pop ( explode ( '/' , $_SERVER["SCRIPT_NAME"] ) ) ;
		$uil = strtoupper($this->interface_language) ;
		$lil = strtolower($this->interface_language) ;

		header('Content-type: text/html; charset=utf-8');
		print "<!DOCTYPE html>\n" ;
		print '<html lang="en"><head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/magnustools/resources/dist3/css/bootstrap.min.css" rel="stylesheet">
		<script type="text/javascript" src="/magnustools/resources/js/jquery/jquery-1.10.1.min.js"></script>
		<script src="/magnustools/resources/dist3/js/bootstrap.min.js"></script>
		<style>label { font-weight:normal} </style>
		</head><body>' ;
		print '<div id="menubar" class="navbar navbar-static-top"><div class="navbar-inner"><div class="container">' ;
		print "<table ><tr><td rowspan='2' nowrap><h1 style='margin-top:0px;margin-bottom:0px;padding-bottom:0px;padding-right:5px'>" . $this->loc['toolname'] . '</h1></td>' ;
		print "<td valign='bottom' width='100%'><small>{$this->loc['subtitle']}</small>" ;
		print " [<a href='http://meta.wikipedia.org/wiki/CatScan2/{$lil}'><small>{$this->loc['manual']}</small></a>]" ;
		print " [<a href='{$this->i18n_url}#{$uil}'><small>{$this->loc['interface_text']}</small></a>]</td></tr><tr><td>" ;
		print "{$this->loc['interface_language']} : " ;
		foreach ( $this->available_interface_languages AS $l ) {
			if ( $l == $this->interface_language ) print " [<small><b>" . strtoupper ( $l ) . "</b></small></a>]" ;
			else print " [<a href='$script?interface_language=$l'><small>" . strtoupper ( $l ) . "</small></a>]" ;
		}
		print "</td></table>" ;
		print "</div></div>" ;
//print "<p><i>NOTE :</i> Due to database server issues, some projects (Commons, ru.wikipedia, and others) don not work at the moment.</p>" ;
		
		$cats = implode ( "\n" , $this->basecats_orig ) ;
		$ncats = implode ( "\n" , $this->negcats_orig ) ;
		
		
		// Show the entry form
//		print "<div style='margin:10pt;font-size:14pt;color:red'>If you're upset because you wanted WikiSense and got this page instead, complain to <a href='https://wikitech.wikimedia.org/wiki/User:Daniel_Kinzler'>Daniel</a>, not me!</div>" ;
		print '<div class="container"><div class="row">' ;
		print "<div style='margin-top:10px;font-size:14pt;border-left:10px solid red;padding-left:10px'><b>This tool is being rewritten!</b> Some options are <s>not available</s> at the moment, but will be soon.</div>" ;
		print '<form class="form-inline" method="post" action="./' . $script . '" role="form">' ;
		print '<table border="1px solid #DDDDDD" style="margin-top:5px" cellspacing=0 cellpadding=2 >' ;
		print "<tr><th>" . $this->loc['language'] . "</th>" ;
		print "<td><input type='text' name='language' value='{$this->language}' /></td></tr>" ;
		print "<tr><th>" . $this->loc['project'] . "</th>" ;
		print "<td><input type='text' name='project' value='{$this->project}' /></td></tr>" ;
		print "<tr><th>" . $this->loc['depth'] . "</th>" ;
		print "<td><input type='text' name='depth' value='{$this->depth}' />{$this->loc['depth_explanation']}</td></tr>" ;
		print "<tr><th>" . $this->loc['categories'] . "</th>" ;
		print "<td><textarea name='categories' rows='3' cols='80'>{$cats}</textarea></td></tr>" ;

		print "<tr><th>" . $this->loc['negative_categories'] . "</th>" ;
		print "<td><textarea name='negcats' rows='2' cols='80'>{$ncats}</textarea></td></tr>" ;

		$combs = array ( 'list' , 'subset' , 'union' , 'diff' , 'atleast' ) ;
		print "<tr><th>" . $this->loc['combination'] . "</th><td>" ;
		foreach ( $combs AS $c ) {
			$checked = isset ( $this->comb[$c] ) ? 'checked' : '' ;
			$name = $this->loc["comb_$c"] ;
			print "<input type='checkbox' name='comb[$c]' value='1' id='comb_$c' $checked /><label for='comb_$c'>" ;
			if ( $c == 'list' or $c == 'diff' or $c == 'atleast' ) print "<s>" ;
			print $name ;
			if ( $c == 'list' or $c == 'diff' or $c == 'atleast' ) print "</s>" ;
			print "</label>" ;
		}
		print " <input type='text' size='3' name='atleast_count' value='{$this->atleast_count}' />" ;
		print "</td></tr>" ;
		
		
		
		print "<tr><th>" . $this->loc['namespaces'] . "</th><td>" ;
		print "<div>" ;
		print $this->getNamespaceTable ( 0 , 99 ) ;
		print "</div>" ;
		print "<div style='margin-top:5px;'>" ;
		print $this->getNamespaceTable ( 100 , PHP_INT_MAX ) ;
		print "</div>" ;


		print '</td></tr>' ;
		
		print "<tr><th>" . $this->loc['show_redirects'] . "</th><td>" ;
		print $this->getRadioGroup ( 'show_redirects' , $this->show_redirects , array ( 'both'=>$this->loc['edits_both'] , 'yes'=>$this->loc['edits_yes'] , 'no'=>$this->loc['edits_no'] ) ) ;
		print '</td></tr>' ;
		
		
		
		print "<tr><th>" . $this->loc['templates'] . "</th><td>" ;
		print "<table><tr>" ;
		$first = true ;
		foreach ( $this->templates AS $t ) {
			$t2 = array_pop ( explode ( '_' , $t ) ) ;
			$n = "templates_use_talk_$t2" ;
			$tut_checked = $this->$n ? 'checked' : '' ;
			if ( $first ) $b = '' ;
			else $b = 'style="border-left:1px solid black; padding-left:2px"' ;
			print "<td $b>{$this->loc[$t]}<br/><textarea name='$t' cols='30' rows='4'>" . implode ( "\n" , $this->$t ) . "</textarea>" ;
			print "<br/><input type='checkbox' name='templates_use_talk_$t2' id='templates_use_talk_$t2' value='1' $tut_checked />" ;
			print "<label for='templates_use_talk_$t2'>{$this->loc['templates_use_talk_instead']}</label>" ;
			print "</td>" ;
			$first = false ;
		}
		print "</tr></table>" ;
		print '</td></tr>' ;


		print "<tr><th>" . $this->getloc('outlinks') . "</th><td>" ;
		print "<table><tr>" ;
		$first = true ;
		foreach ( $this->outlinks AS $o ) {
			$o2 = array_pop ( explode ( '_' , $o ) ) ;
			if ( $first ) $b = '' ;
			else $b = 'style="border-left:1px solid black; padding-left:2px"' ;
			print "<td $b>".$this->getloc($o)."<br/><textarea name='$o' cols='30' rows='4'>" . implode ( "\n" , $this->$o ) . "</textarea>" ;
			print "</td>" ;
			$first = false ;
		}
		print "</tr></table>" ;
		print '</td></tr>' ;



		$edits = array ( 'bots' , 'anons' , 'flagged' ) ;
		print "<tr><th>" . $this->loc['edits'] . "</th><td>" ;
		foreach ( $edits AS $num => $e ) {
			print "<span" . ($num>0?" style='margin-left:20px;padding-left:20px;border-left:1px solid #EEE'":'') . ">" ;
			print $this->loc["edit_$e"] . '&nbsp;' ;
			print $this->getRadioGroup ( 'edits['.$e.']' , $this->edits[$e] , array ( 'both'=>$this->loc['edits_both'] , 'yes'=>$this->loc['edits_yes'] , 'no'=>$this->loc['edits_no'] ) ) ;
			print "</span>" ;
		}
		print '</td></tr>' ;
		
		print "<tr><th>" . $this->loc['last_change'] . "</th><td>" ;
		
		print $this->getInputGroup ( $this->loc['before'] , 'before' , $this->loc['date_format'] , $this->before ) ;
		print $this->getInputGroup ( $this->loc['after'] , 'after' , $this->loc['date_format'] , $this->after ) ;
		print $this->getInputGroup ( $this->getloc('max_age') , 'max_age' , $this->getloc('max_age_format') , $this->max_age ) ;
		print "<label><input type='checkbox' name='only_new' value='1' " . ($this->only_new?'checked':'') . " /> " . $this->getloc('only_new') ."</label>" ;
		print '</td></tr>' ;
		
		print "<tr><th>" . $this->loc['size'] . "</th><td>" ;
		print $this->getInputGroup ( $this->loc['larger'] , 'larger' , $this->loc['size_explanation'] , $this->larger ) ;
		print $this->getInputGroup ( $this->loc['smaller'] , 'smaller' , $this->loc['size_explanation'] , $this->smaller ) ;
		print '</td></tr>' ;
		
		print "<tr><th>" . $this->loc['link_number'] . "</th><td>" ;
		print $this->getInputGroup ( $this->loc['minlinks'] , 'minlinks' , '' , $this->minlinks ) ;
		print $this->getInputGroup ( $this->loc['maxlinks'] , 'maxlinks' , '' , $this->maxlinks ) ;
		print $this->loc['links_explanation'] ;
		print '</td></tr>' ;
		

		$sred = $this->show_redlinks ? 'checked' : '' ;
		$sredo = $this->show_redlinks_only ? 'checked' : '' ;
		$rtr = $this->remove_template_redlinks ? 'checked' : '' ;
		$aro = $this->article_redlinks_only ? 'checked' : '' ;
		print "<tr><th>" . $this->loc['redlinks'] . "</th><td>" ;
		print '<div class="btn-group" data-toggle="buttons">' ;
		print "<label class='btn btn-primary ".($sred=='checked'?'active':'')."'><input type='checkbox' name='show_redlinks' value='1' $sred /> ".$this->loc['show_redlinks']."</label>" ;
		print "<label class='btn btn-primary ".($sredo=='checked'?'active':'')."'><input type='checkbox' name='show_redlinks_only' value='1' $sredo /> ".$this->loc['show_redlinks_only']."</label>" ;
		print "<label class='btn btn-primary ".($rtr=='checked'?'active':'')."'><input type='checkbox' name='remove_template_redlinks' value='1' $rtr /> ".$this->loc['remove_template_redlinks']."</label>" ;
		print "<label class='btn btn-primary ".($aro=='checked'?'active':'')."'><input type='checkbox' name='article_redlinks_only' value='1' $aro /> ".$this->loc['article_redlinks_only']."</label>" ;
		print '</div>' ;
		print $this->getInputGroup ( $this->loc['min_redlink_count'] , 'min_redlink_count' , '' , $this->min_redlink_count ) ;
		
		
		
		print '</td></tr>' ;

		$topc = $this->show_topcats ? 'checked' : '' ;
		$stco = $this->show_topcats_only ? 'checked' : '' ;
		$tcnt = $this->topcats_no_talk ? 'checked' : '' ;
		print "<tr><th><s>" . $this->getloc('topcats') . "</s></th><td><s>" ;
		print "<table><tr>" ;
		print "<td><input type='checkbox' name='show_topcats' value='1' id='show_topcats' $topc /><label for='show_topcats'><s>" . $this->getloc('show_topcats') . "</label></td>" ;
		print "<td><input type='checkbox' name='show_topcats_only' value='1' id='show_topcats_only' $stco /><label for='show_topcats_only'><s>" . $this->getloc('show_topcats_only') . "</label></td>" ;
		print "<td><input type='checkbox' name='topcats_no_talk' value='1' id='topcats_no_talk' $tcnt /><label for='topcats_no_talk'><s>" . $this->getloc('topcats_no_talk_pages') . "</label></td>" ;
		print "</tr><tr>" ;
		print "<td colspan='2'>" . $this->getloc('min_topcat_count') . " <input type='text' size='3' name='min_topcat_count' value='{$this->min_topcat_count}' /></td>" ;
		print "</tr></table>" ;
		print '</s></td></tr>' ;


		$get_q = $this->get_q ? 'checked' : '' ;
		print "<tr><th>" . $this->getloc('wikidata') . "</th><td>" ;
		print "<table><tr>" ;
		print "<td><input type='checkbox' name='get_q' value='1' id='get_q' $get_q /><label for='get_q'>" . $this->getloc('get_q') . "</label></td>" ;
//		print "<td><input type='checkbox' name='show_topcats_only' value='1' id='show_topcats_only' $stco /><label for='show_topcats_only'><s>" . $this->getloc('show_topcats_only') . "</label></td>" ;
//		print "<td><input type='checkbox' name='topcats_no_talk' value='1' id='topcats_no_talk' $tcnt /><label for='topcats_no_talk'><s>" . $this->getloc('topcats_no_talk_pages') . "</label></td>" ;
//		print "</tr><tr>" ;
//		print "<td colspan='2'>" . $this->getloc('min_topcat_count') . " <input type='text' size='3' name='min_topcat_count' value='{$this->min_topcat_count}' /></td>" ;
		print "</tr></table>" ;
		print '</td></tr>' ;

		
		$sa = $this->sort_order == 'ascending' ? 'checked' : '' ;
		$sd = $this->sort_order == 'descending' ? 'checked' : '' ;
		print "<tr><th><s>" . $this->loc['sort'] . "</s></th><td>" ;
		foreach ( $this->sort_modes AS $sm ) {
			if ( $this->sort_by == $sm ) $chk = 'checked' ;
			else $chk = '' ;
			if ( !isset ( $this->loc['sort_by_'.$sm] ) ) $txt = "<i>untranslated</i> sort_by_$sm" ;
			else $txt = $this->loc['sort_by_'.$sm] ;
			print "<input type='radio' name='sortby' value='$sm' id='sortby_$sm' $chk /><label for='sortby_$sm'><s>" . $txt . "</s></label>" ;
		}
		print ' &nbsp; ' ;
		print "<input type='radio' name='sortorder' value='ascending' id='sort_ascending' $sa /><label for='sort_ascending'><s>" . $this->getloc('sort_ascending') . "</s></label>" ;
		print "<input type='radio' name='sortorder' value='descending' id='sort_descending' $sd /><label for='sort_descending'><s>" . $this->getloc('sort_descending') . "</s></label>" ;
		print '</td></tr>' ;
		
		print "<tr><th>" . $this->getloc('format') . "</th><td>" ;

		print $this->getRadioGroup ( 'format' , $this->format , $this->formats ) ;
/*
		foreach ( $this->formats AS $k => $v ) {
			$checked = $this->format == $k ? 'checked' : '' ;
			print "<input type='radio' name='format' value='$k' id='format_$k' $checked /><label for='format_$k'>$v</label>" ;
		}*/
		$eid = $this->ext_image_data ? 'checked' : '' ;
		$fud = $this->file_usage_data ? 'checked' : '' ;
		print " <input type='checkbox' name='ext_image_data' id='ext_image_data' value='1' $eid><label for='ext_image_data'><s>".$this->getloc('ext_image_data')."</s></label>" ;
		print " <input type='checkbox' name='file_usage_data' id='file_usage_data' value='1' $fud><label for='file_usage_data'><s>".$this->getloc('file_usage_data')."</s></label>" ;
		print '</td></tr>' ;
		
		print "<tr><th></th><td><input class='btn btn-primary' type='submit' name='doit' value='".$this->getloc('doit')."' /></td></tr>" ;
		
		if ( $this->doIt() ) {
			$url = $this->url_get() ;
			$s = $this->loc['query_url'] ;
			$s = str_replace ( '$1' , "$url&doit=1" , $s ) ;
			$s = str_replace ( '$2' , $url , $s ) ;
			print "<tr><th>" . $this->loc['th_query_url'] ;
			print "</th><td>$s</td></tr>" ;
		}
		
		print '</table>' ;
		print "<input type='hidden' name='interface_language' value='{$this->interface_language}' />" ;
		print '</form>' ;
		print '</div></div>' ;
		
		
//		$this->prettyOutput () ;  // TODO
		
		print '</body></html>' ;
	}

	function url_get () {
		if ( !isset($_SERVER["SERVER_NAME"]) ) return '' ; // Command line use
		$url = 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["SCRIPT_NAME"] . "?" ;
		$o = array () ;
		foreach ( $_REQUEST AS $k => $v ) {
			if ( substr ( $k , 0 , 1 ) == '_' ) continue ;
			if ( $k == 'doit' ) continue ;
			if ( $k == 'interface_language' and $v == 'en' ) continue ;
			if ( $k == 'language' and $v == $this->interface_language ) continue ;
			if ( $k == 'ext_image_data' and !isset($this->ns[6]) ) continue ;
			if ( $k == 'file_usage_data' and !isset($this->ns[6]) ) continue ;
			
			if ( substr ( $k , 0 , 4 ) == 'tusc' ) continue ;
			
			if ( isset ( $this->default_parameter[$k] ) and $v == $this->default_parameter[$k] ) continue ;
			if ( is_array ( $v ) ) {
				if ( count ( $v ) == 0 ) continue ;
				if ( $k == 'ns' and $v == array ( 0 => 1 ) ) continue ;
				if ( $k == 'comb' and $v == array ( 'subset' => 1 ) ) continue ;
				$w = array () ;
				foreach ( $v AS $a => $b ) {
					$w[] = $k . '%5B' . $a . '%5D=' . urlencode ( $b ) ;
				}
				$v = implode ( '&' , $w ) ;
				$o[] = $v ;
			} else {
				$v = urlencode ( trim ( $v ) ) ;
				if ( $v == '' ) continue ;
				$o[] = $k . "=" . $v ;
			}
		}
		$url .= implode ( '&' , $o ) ;
		return $url ;
	}
	
	function getCatTree ( $cat_orig , $redirects ) {
		$cat = explode ( '|' , $cat_orig ) ;
		if ( count ( $cat ) == 1 ) $cat[] = $this->depth ;
		$cat[0] = trim ( $cat[0] ) ;
		$cat[1] *= 1 ;
		if ( $cat[0] == '' ) continue ;
		$pagelist = new Pagelist ;
		$pagelist->loadCategoryTree ( array (
			'language'=>$this->language , 
			'project'=>$this->project ,
			'root'=>$cat[0] ,
			'depth'=>$cat[1] ,
			'namespaces'=>array_keys($this->ns) ,
			'redirects'=>$redirects
		) ) ;
		return $pagelist ;
	}
	
	function runQueryCategories () {
		$pl = array() ;
		$redirects = $this->show_redirects ;
		if ( $redirects == 'yes' ) $redirects = 'only' ;
		if ( $redirects == 'no' ) $redirects = 'none' ;
		foreach ( $this->basecats AS $cat ) {
			$pl[] = $this->getCatTree ( $cat , $redirects ) ;
		}
		
		if ( count($pl) == 0 ) return ; // Paranoia

		foreach ( $this->comb AS $mode => $dummy ) {
			if ( count($pl) == 1 ) { // Only one
				$this->results[$mode] = $pl[0]->duplicate() ;
			} else if ( $mode == 'subset' || $mode == 'union' ) {
				$last = $pl[0] ;
				$combination = new Pagelist ;
				foreach ( $pl AS $k => $v ) {
					if ( $k == 0 ) continue ;
					$combination->$mode ( $last , $v ) ;
					$last = $combination ;
				}
				$combination->language = $this->language ;
				$combination->project = $this->project ;
				$this->results[$mode] = $combination ;
			}
			// TODO Catlist, Diff, AtLeast
		}
	}
	
	
	function runOutlinks () {
		foreach ( array ( 'yes' , 'any' , 'no' ) AS $outlink_mode ) {
			$key = 'outlinks_' . $outlink_mode ;
			if ( count($this->$key) == 0 ) continue ;


			$pp = new PagePile ;
			$pp->createNewUnregisteredPile ( $this->language , $this->project ) ;
			$pp->addPagesLinkedFrom ( $this->$key , ($outlink_mode=='yes'?'all':'any') ) ;
			
			$pl = new Pagelist ;
			$pl->loadPagePile ( $pp ) ;
		
			foreach ( $this->results AS $mode => $res ) {
				if ( $outlink_mode == 'no' ) {
					$this->results[$mode]->only_in_list_1 ( $res , $pl ) ;
				} else {
					$this->results[$mode]->subset ( $res , $pl ) ;
				}
			}
		}
		
	}

	function runQueryNegativeCategories () {
		if ( count($this->negcats) == 0 ) return ;
		
		$neg_cat = 'init' ;
		foreach ( $this->negcats AS $cat ) {
			$pl = $this->getCatTree ( $cat , 'any' ) ;
			if ( $neg_cat == 'init' ) $neg_cat = $pl ;
			else $neg_cat->union ( $neg_cat , $pl ) ;
		}
		
		foreach ( $this->results AS $mode => $res ) {
			$this->results[$mode]->only_in_list_1 ( $res , $neg_cat ) ;
		}
	}
	
	function runAllTemplates () {
		if ( count($this->templates_yes) == 0 ) return ;
		foreach ( $this->templates_yes AS $k => $v ) $this->templates_yes[$k] = trim ( ucfirst ( $v ) ) ;
		
		$pl = 'init' ;
		foreach ( $this->templates_yes AS $template ) {
			$pl2 = new Pagelist ;
			$pl2->loadTemplates ( array ( 'language'=>$this->language , 'project'=>$this->project , 'templates'=>array($template) , 'talk2main'=>$this->templates_use_talk_yes ) ) ;
			if ( $pl == 'init' ) $pl = $pl2 ;
			else $pl->subset ( $pl , $pl2 ) ;
		}
		
		foreach ( $this->results AS $mode => $res ) {
			$this->results[$mode]->subset ( $res , $pl ) ;
		}
	}
	
	function runAnyTemplates () {
		if ( count($this->templates_any) == 0 ) return ;
		foreach ( $this->templates_any AS $k => $v ) $this->templates_any[$k] = ucfirst ( trim ( $v ) ) ;
		
		$pl = new Pagelist ;
		$pl->loadTemplates ( array ( 'language'=>$this->language , 'project'=>$this->project , 'templates'=>$this->templates_any , 'talk2main'=>$this->templates_use_talk_any ) ) ;
		
		foreach ( $this->results AS $mode => $res ) {
			$this->results[$mode]->subset ( $res , $pl ) ;
		}
	}
	
	function runNegativeTemplates () {
		if ( count($this->templates_no) == 0 ) return ;
		foreach ( $this->templates_no AS $k => $v ) $this->templates_no[$k] = ucfirst ( trim ( $v ) ) ;
		
		$pl = new Pagelist ;
		$pl->loadTemplates ( array ( 'language'=>$this->language , 'project'=>$this->project , 'templates'=>$this->templates_no , 'talk2main'=>$this->templates_use_talk_no ) ) ;
		
		foreach ( $this->results AS $mode => $res ) {
			$this->results[$mode]->only_in_list_1 ( $res , $pl ) ;
		}
	}
	
	function runMiscFilters () {
	
		$runit = false ;
		foreach ( array('before','after','max_age','larger','smaller','minlinks','minlinks') AS $v ) {
			if ( trim($this->$v) != '' ) $runit = true ;
		}
		if ( $this->edits['bots'] != 'both' ) $runit = true ;
		if ( $this->edits['anons'] != 'both' ) $runit = true ;
		if ( $this->edits['flagged'] != 'both' ) $runit = true ;
		if ( !$runit ) return ; // Don't bother

		$db = openDB ( $this->language , $this->project ) ;

		foreach ( $this->results AS $mode => $res ) {
			$tmp = new Pagelist() ;
			$tmp->blankFile ( $this->language , $this->project ) ;
			while ( $a = $res->getNextBatch() ) {
				$byns = array() ;
				foreach ( $a AS $v ) $byns[$v[1]][$v[0]] = $db->real_escape_string ( $v[0] ) ;
				unset ( $a ) ;
				foreach ( $byns AS $ns => $pages ) {
					$tables = array ( 'page'=>'page' ) ;
					$where = array ( "page_namespace=$ns" , "page_title IN ('".implode("','",$pages)."')" ) ;
					$having = array() ;
					
					if ( trim($this->larger) != '' ) $where[] = 'page_len>='.($this->larger*1) ;
					if ( trim($this->smaller) != '' ) $where[] = 'page_len<='.($this->smaller*1) ;
					if ( $this->show_redirects == 'yes' ) $where[] = 'page_is_redirect=1' ;
					if ( $this->show_redirects == 'no' ) $where[] = 'page_is_redirect=0' ;
					
					if ( trim($this->before).trim($this->after).trim($this->max_age) != '' ) {
						$tables['revision'] = 'revision' ;
						if ( trim($this->before) != '' ) $where[] = 'rev_timestamp <= "' . get_db_safe ( $this->before ) . '"' ;
						if ( trim($this->after) != '' ) $where[] = 'rev_timestamp >= "' . get_db_safe ( $this->after ) . '"' ;
						if ( trim($this->max_age) != '' ) {
							$d = time() - ( $this->max_age * 60 * 60) ; // max_age in hours
							$d = date ( 'YmdHis' , $d ) ;
							$where[] = 'rev_timestamp >= "' . $d . '"' ;
						}
						if ( $this->only_new ) {
							$where[] = 'rev_page = page_id' ;
							$where[] = 'rev_parent_id = 0' ;
						} else {
							$where[] = 'rev_id = page_latest' ;
						}
					}
					
					if ( $this->minlinks*1 > 0 ) {
						$having[] = "(select count(*) FROM pagelinks WHERE pl_from=page_id) >= " . $this->minlinks*1 ;
					}
					
					if ( $this->maxlinks*1 > 0 ) {
						$having[] = "(select count(*) FROM pagelinks WHERE pl_from=page_id) <= " . $this->maxlinks*1 ;
					}
					
					if ( $this->edits['anons'] != 'both' ) {
						if ( $this->edits['anons'] == 'yes' ) $where[] = "EXISTS (SELECT * FROM revision WHERE rev_id=page_latest AND rev_user=0)" ;
						else $where[] = "EXISTS (SELECT * FROM revision WHERE rev_id=page_latest AND rev_user!=0)" ;
					}

					if ( $this->edits['bots'] != 'both' ) {
						if ( $this->edits['bots'] == 'yes' ) $where[] = "EXISTS (SELECT * FROM revision,user_groups WHERE rev_id=page_latest AND rev_user=ug_user AND ug_group='bot')" ;
						else "EXISTS (SELECT * FROM revision,user_groups WHERE and rev_id=page_latest AND rev_user=ug_user AND ug_group!='bot')" ;
					}

					if ( $this->edits['flagged'] != 'both' ) {
						if ( $this->edits['flagged'] == 'yes' ) $where[] = "EXISTS (SELECT * FROM flaggedpages WHERE page_id=fp_page_id AND fp_stable=page_latest AND fp_reviewed=1)" ;
						else $where[] = "EXISTS (SELECT * FROM flaggedpages WHERE page_id=fp_page_id AND fp_stable=page_latest AND fp_reviewed!=1)" ;
					}
					
					$sql = "SELECT DISTINCT page_namespace,page_title FROM " . implode(',',$tables) . " WHERE " . implode(' AND ',$where) ;
					if ( count($having)>0 ) $sql .= " HAVING " . implode ( ' AND ' , $having ) ;
//					print "<pre>$sql</pre>" ;
					if(!$result = $db->query($sql)) {} //error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
					while($r = $result->fetch_object()){
						$tmp->appendPage ( array ( $r->page_title , $r->page_namespace ) ) ;
					}
					
				}
			}
			$res->swap ( $tmp ) ;
		}

	}
	
	
	
	function runRedlinks () {
		if ( !$this->show_redlinks ) return ;
		
		$params = array ( 'min' => $this->min_redlink_count ) ;
		if ( $this->remove_template_redlinks ) $params['no_templates'] = 1 ;
		if ( $this->article_redlinks_only ) $params['namespaces'] = 0 ;
		$newres = array() ;
		foreach ( $this->results AS $mode => $res ) {
			$pl = $res->getRedlinks ( $params ) ;
			
			$newres["redlinks.$mode"] = $pl ;
		}
		
		if ( $this->show_redlinks_only ) $this->results = $newres ;
		else $this->results += $newres ; // TODO check
		
	}
	
	
	
	function runQuery () {
		if ( !$this->doIt() ) return ;

		$this->results = array() ;
		
		$this->runQueryCategories() ;
		$this->runQueryNegativeCategories() ;
		$this->runAllTemplates() ; // TODO all templates templates_use_talk_yes
		$this->runAnyTemplates() ;
		$this->runNegativeTemplates() ;
		$this->runOutlinks() ;
		$this->runMiscFilters() ;
		$this->runRedlinks() ;
		
		
//		print "<pre>TOTAL : " . count($this->results['subset']->asRawArray()) . "</pre>" ;
		
		$this->query_mem = round(memory_get_peak_usage(true)/100000)/10 ;
		$this->query_sec = microtime(true) - $this->start_time ;
		
		if ( $this->format == 'html' ) {
			print '<div class="container"><div class="row">' ;
			print "<p>Query took {$this->query_sec} seconds. {$this->query_mem} MB memory used.</p>" ;
			print "</div></div>" ;
		}
		
		$this->outputResults() ;

//		cleanOldOmniscanTempFiles() ; // Cleanup
	}
	
	function annotateResults ( $res ) {
		$ret = getOmiscanTempFile() ;
		$db = openDB ( $this->language , $this->project ) ;
		$fh = fopen ( $ret , 'w' ) ;
		$dbwd = '' ;
		$site = '' ;
		if ( $this->get_q ) {
			$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
			if ( $this->project == 'wikipedia' ) $site = $this->language . 'wiki' ;
			else $site = $this->language . $this->project ;
			$site = $dbwd->real_escape_string ( $site ) ;
		}
		while ( $a = $res->getNextBatch() ) {
			foreach ( $a AS $v ) {
				$j = array ( 'title'=>$v[0] , 'namespace'=>$v[1] ) ;

				if ( count($v) == 3 ) { // Redlinks!
					$j['links'] = $v[2]*1 ;
				} else {
				
					$sql = "SELECT * FROM page where page_namespace=" . $v[1] . " AND page_title='" . $db->real_escape_string ( $v[0] ) . "' LIMIT 1" ;
					if(!$result = $db->query($sql)) {} //error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
					while($r = $result->fetch_object()){
						$j['id'] = $r->page_id ;
						$j['len'] = $r->page_len ;
						$j['touched'] = $r->page_touched ;
					}
				
				}
				
				if ( $this->get_q ) {
					$j['q'] = '' ;
					$sql = "select ips_item_id from wb_items_per_site where ips_site_id='$site' and ips_site_page='" . $dbwd->real_escape_string ( str_replace('_',' ',$v[0]) ) . "'" ;
					if(!$result = $dbwd->query($sql)) {} //error('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
					while($r = $result->fetch_object()){
						$j['q'] = 'Q'.$r->ips_item_id ;
					}
				}
				
				$j = json_encode($j) ;
//				print "<pre>$j</pre>" ;
				fwrite ( $fh , "$j\n" ) ;
			}
		}
		fclose ( $fh ) ;
		return $ret ;
	}



	// OUTPUT FUNCTIONS


	function printXML () { // TODO
		header('Content-type: text/xml; charset=utf-8');
		$this->printXMLsub ( $this->result ) ;
	}
	
	function printPHP () { // TODO
		header('Content-type: application/serialized_PHP_variable');
		print serialize ( $this->result ) ;
	}
	
	function outputResults () {

		$type = $this->format ;
		
		if ( $type == 'csv' or $type == 'tsv' or $type == 'wiki' or $type == 'gallery' ) {
			header('Content-type: text/plain; charset=utf-8');
		} else if ( $type == 'pagepile' ) {
			return $this->printPagepile() ;
		} else if ( $type == 'html' ) {
			print '<div class="container"><div class="row">' ;
			print "<h1><a name='results'>" . $this->getloc('results') . "</a></h1>" ;
		} else if ( $type == 'xml' ) {
			return printXML() ;
		} else if ( $type == 'php' ) {
			return printPHP() ;
		} else if ( $type == 'json' ) {
			header('Content-type: application/x-json; charset=utf-8');
			if ( isset ( $_REQUEST['callback'] ) ) print $_REQUEST['callback'] . "(" ;
			print '{"n":"result","a":{"querytime_sec":' . $this->query_sec . '},"*":[' ;
		} else {
			// TODO unknown type error
		}

		foreach ( $this->results AS $mode => $res ) {
			$tmp1 = $this->annotateResults ( $res ) ;
			$this->prettyOutput ( $mode , $tmp1 ) ;
			unlink ( $tmp1 ) ;
		}
		
		if ( $type == 'html' ) {
			print "</div></div>" ;
			
			if ( preg_match ( '/\/test\//' , $_SERVER["REQUEST_URI"] ) ) {
				print "<hr/><h2>Debugging data, ignore</h2>" ;
				print "<pre>" ;
				print_r ( $this ) ;
				print "</pre>" ;
			}
			
		} else if ( $type == 'json' ) {
			print ']}' ;
			if ( isset ( $_REQUEST['callback'] ) ) print ");" ;
		}
		
	}
	
	function printPagepile () {
		$pp = new Pagepile ;
		$pp->createNewPile ( $this->language , $this->project ) ;
		$pp->beginTransaction() ;
		foreach ( $this->results AS $mode => $res ) {
			$tmp1 = $this->annotateResults ( $res ) ;

			$fh = fopen ( $tmp1 , 'r' ) ;
			while ( !feof($fh) ) {
				$j = trim ( fgets ( $fh ) ) ;
				if ( $j == '' ) continue ;
				$j = json_decode ( $j ) ;
				$pp->addPage ( $j->title , $j->namespace ) ;
			}
			fclose ( $fh ) ;
			unlink ( $tmp1 ) ;
			break ; // Only one!
		}
		$pp->commitTransaction() ;
		$pp->setTrackFromUrl ( 'CatScan 2' ) ;
		$pp->printAndEnd() ;
	}


	
	function makeCSV ( $s ) {
		return str_replace ( '"' , '\\"' , $s ) ;
	}
	
	
	// Handles result output for HTML, CSV, TSV, Wiki
	function prettyOutput ( $mode , $tmp1 ) {
		
		$type = $this->format ;
		
		$baseurl = "http://{$this->language}.{$this->project}.org/wiki" ;
		
		$header = array ( 'title'=>'h_title' , 'id'=>'h_id' , 'namespace'=>'h_namespace' , 'len'=>'h_len' , 'touched'=>'h_touched' ) ;
		if ( $this->get_q ) $header['q'] = 'h_wikidata' ;
		if ( preg_match ( '/^redlinks\./' , $mode ) ) $header = array ( 'title'=>'h_title' , 'links'=>'link_number' , 'namespace'=>'h_namespace' ) ;
		if ( $type != 'html' ) $header['nstext'] = 'h_nstext' ;
		foreach ( $header AS $k => $v ) {
			$header[$k] = $this->getloc($v) ;
		}

		// Print header
		if ( isset ( $this->loc['comb_'.$mode] ) ) $ty = $this->loc['comb_'.$mode] ;
		else $ty = $mode ;
//		if ( $mode == 'atleast' ) $ty .= " (" . $c['a']['atleast'] . ")" ;
		if ( $type == 'html' ) {
			print "<h2>$ty</h2>" ;
			print "<table class='table table-condensed table-striped'>" ;
			print "<thead><tr><th>" . implode("</th><th>",$header) . "</th></tr></thead>" ;
			print "<tbody>\n" ;
		} else if ( $type == 'csv' ) {
			print "|$mode\n" ;
			print '"' . implode('","',array_keys($header)) . "\"\n" ;
		} else if ( $type == 'tsv' ) {
			print "|$mode\n" ;
			print implode("\t",array_keys($header)) . "\n" ;
		} else if ( $type == 'json' ) {
			print '{"n":"combination","a":{"type":"' . $mode . '","*":[' ;
		} else if ( $type == 'wiki' ) {
			print "== $ty ==\n" ;
			print "{| border='1'\n" ;
			print '!' . implode(" !! ",$header) . "\n" ;
		}
		
		// Print rows
		$cnt = 0 ;
		$fh = fopen ( $tmp1 , 'r' ) ;
		while ( !feof($fh) ) {
			$j = trim ( fgets ( $fh ) ) ;
			if ( $j == '' ) continue ;
			$j = json_decode ( $j ) ;
//			print "<pre>" ; print_r ( $j ) ; print "</pre>" ;

			$j->nstext = '' ;
			$ns = $j->namespace ;
			if ( isset ( $this->namespaces[$ns] ) ) $j->nstext = $this->namespaces[$ns] ;
			$prefixed_title = str_replace('_',' ',$j->title) ;
			if ( $ns != 0 and $j->nstext != '' ) $prefixed_title = $j->nstext . ":" . $prefixed_title ;
			
			
			if ( $type == 'html' ) {
				print "<tr>" ;
				foreach ( $header AS $k => $v ) {
					print "<td>" ;
					$o = isset($j->$k) ? $j->$k : '' ;
					if ( $k == 'q' ) {
						print "<a href='//www.wikidata.org/wiki/$o' target='_blank'>$o</a>" ;
					} else {
						if ( $k == 'title' ) $o = "<a target='_blank' href='$baseurl/".urlencode(str_replace(' ','_',$prefixed_title))."'>" . $prefixed_title . "</a>" ;
						else if ( $k == 'namespace' ) $o = $j->nstext ;
						print $o ;
					}
					print "</td>" ;
				}
				print "</tr>\n" ;

			} else if ( $type == 'csv' ) {

				$first = true ;
				foreach ( $header AS $k => $v ) {
					$o = $this->makeCSV ( isset($j->$k) ? $j->$k : '' ) ;
					if ( $first ) $first = false ;
					else print ',' ;
					print '"'.$o.'"' ;
				}
				print "\n" ;

			} else if ( $type == 'tsv' ) {

				$first = true ;
				foreach ( $header AS $k => $v ) {
					$o = isset($j->$k) ? $j->$k : '' ;
					if ( $first ) $first = false ;
					else print "\t" ;
					print $o ;
				}
				print "\n" ;

			} else if ( $type == 'wiki' ) {

				print "|-\n" ;
				$first = true ;
				foreach ( $header AS $k => $v ) {
					print '|' ;
					$o = isset($j->$k) ? $j->$k : '' ;
					if ( $first ) $first = false ;
					else print "| " ;
					if ( $k == 'title' ) {
						print " [[$prefixed_title]] " ;
					} else if ( $k == 'q' ) {
						if ( $o == '' ) print " — " ;
						else print " [[:wikidata:$o|$o]] " ;
					} else {
						print " $o " ;
					}
				}
				print "\n" ;

			} else if ( $type == 'json' ) {

				if ( $cnt > 0 ) print ',' ;
				print '{"n":"page","a":{' ;
				$tmp = array() ;
				foreach ( $header AS $k => $v ) {
					$o = isset($j->$k) ? $j->$k : '' ;
					$tmp[] = json_encode($k).":".json_encode($o) ;
				}
				print implode ( ',' , $tmp ) ;
				print '}}' ;

			}

			$cnt++ ;
		}

		fclose ( $fh ) ;

		// Footer
		if ( $type == 'html' ) {
			print "</tbody>" ;
			print "<tfoot><tr>" ;
			print "<th>count</th><td colspan=" . (count($header)-1) . ">$cnt</td>" ;
			print "</tr></tfoot>" ;
			print "</table>" ;
		} else if ( $type == 'json' ) {
			print ']}}' ;
		} else if ( $type == 'wiki' ) {
			print "|}\n" ;
		}
		
	}
	
	function printXMLsub ( &$d ) {
		if ( !is_array ( $d ) ) {
			print htmlspecialchars ( $d , ENT_COMPAT , 'UTF-8' ) ;
			return ;
		}
		if ( !isset ( $d['n'] ) ) return ;
		
		print '<' . $d['n'] ;
		if ( isset ( $d['a'] ) ) {
			foreach ( $d['a'] AS $a => $v ) {
				print ' ' . $a . '="' ;
				print htmlspecialchars ( $v , ENT_COMPAT , 'UTF-8' ) ;
				print '"' ;
			}
		}
		
		if ( isset ( $d['*'] ) and count ( $d['*'] ) > 0 ) {
			print ">" ;
			foreach ( $d['*'] AS $s ) {
				$this->printXMLsub ( $s ) ;
			}
			print "</" . $d['n'] . ">" ;
		} else print " />" ;
	}
	

}


//header('Content-type: text/plain; charset=utf-8');
$cs = new CatScan3 ;
$cs->run() ;

?>